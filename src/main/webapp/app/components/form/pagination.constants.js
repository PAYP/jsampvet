(function() {
    'use strict';

    angular
        .module('jSampVetApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
