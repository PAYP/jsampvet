/**
 * Data Access Objects used by WebSocket services.
 */
package com.samp.app.web.websocket.dto;
