/**
 * View Models used by Spring MVC REST controllers.
 */
package com.samp.app.web.rest.vm;
